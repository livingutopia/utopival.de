---
title: Rückblick 2017
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _banner
            - _fotos
            - _team
            - _berichte
    #         - _team
    #         - _kontakt
    #         - _haendeundkoepfe
---