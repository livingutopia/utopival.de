---
title: Team
team:
  - name: Bakira
    image: user://pages/2017/03.kernteam/_team/bakira.jpg
  - name: Pia
    email: pia.b@utopival.org
    image: user://pages/2017/03.kernteam/_team/pia.jpg
  - name: Helena
    image: user://pages/2017/03.kernteam/_team/helena.jpg
  - name: Juli
    image: user://pages/2017/03.kernteam/_team/juli.jpg
  - name: Ruth
    email: ruth@utopival.org
    image: user://pages/2017/03.kernteam/_team/ruth.jpg
  - name: findus
    email: findus@livingutopia.org
    image: user://pages/2017/03.kernteam/_team/laura.jpg
  - name: Nico
    image: user://pages/2017/03.kernteam/_team/nico.jpg
  - name: chandi
    email: chandi@livingutopia.org
    image: user://pages/2017/03.kernteam/_team/chandi.jpg
  - name: Nadine
    image: user://pages/2017/03.kernteam/_team/nadine.jpg
  - name: Tobi
    image: user://pages/2017/03.kernteam/_team/tobi.jpg
  - name: Tom
    image: user://pages/2017/03.kernteam/_team/tom.jpg
---

## Vorbereitungsteam