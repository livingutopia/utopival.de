---
title: Kooperationen 2017 - Firmen
brands:
  - name: Aronia ORGINAL
    logo: image://brands/aronia-original.svg
    url: http://www.aronia-original.de

  - name: Barnhouse Naturprodukte GmbH
    logo: image://brands/barnhouse.png
    url: http://www.barnhouse.de/

  - name: Bauck GmbH & Co. KG
    logo: image://brands/bauckhof.png
    url: https://www.bauckhof.de/

  - name: beckers bester GmbH
    logo: image://brands/beckers_bester.png
    url: https://www.beckers-bester.de/

  - name: Beltane Naturkost GmbH
    logo: image://brands/beltane.png
    url: http://www.beltane.de/

  - name: Biovegan GmbH
    logo: image://brands/biovegan.png
    url: http://www.biovegan.de

  - name: Wagner Gewürze GmbH
    logo: image://brands/biowagner.png
    url: http://www.biowagner.de/

  - name: Byodo Naturkost GmbH
    logo: image://brands/byodo.png
    url: https://www.byodo.de

  - name: Circle Products GmbH
    logo: image://brands/coffeecircle.png
    url: https://www.coffeecircle.com

  - name: DÖRRWERK GmbH
    logo: image://brands/doerrwerk.png
    url: https://www.doerrwerk.de/

  - name: edding.png
    logo: image://brands/edding.png
    url: https://www.edding.com/de/home/

  - name: EL PUENTE
    logo: image://brands/el_puente.png
    url: https://www.el-puente.de/

  - name: wageswiese GmbH
    logo: image://brands/emils.png
    url: https://www.emils.com/

  - name: ETHIQUABLE Deutschland eG
    logo: image://brands/ethiquable.png
    url: https://www.ethiquable.de

  - name: GEFRO Reformversand Frommlet KG
    logo: image://brands/gefro.png
    url: https://www.gefro.de/

  - name: Govinda Natur GmbH
    logo: image://brands/govinda.png
    url: https://www.govinda-natur.de/

  - name: Wojnar International Ltd.
    logo: image://brands/greenheart.png
    url: http://www.green-heart.at/

  - name: Herbaria Kräuterparadies GmbH
    logo: image://brands/herbaria.png
    url: http://herbaria.com/

  - name: EcoFinia GmbH
    logo: image://brands/ichoc.png
    url: http://ichoc.de/

  - name: Peter Kölln GmbH & Co. KGaA
    logo: image://brands/koelln.png
    url: https://www.koelln.de/

  - name: Kornkraft Naturwaren GmbH
    logo: image://brands/kornkraft.png
    url: http://kornkraft.com

  - name: LeHA GmbH
    logo: image://brands/leha.png
    url: http://schlagfix.com/

  - name: Vekontor HGmbH
    logo: image://brands/wilmersburger.png
    url: https://www.wilmersburger.de/

---

# Spender*innen 2017
die das utopival durch nicht mehr verkäufliche Lebensmittel oder Spenden lebendig werden lassen