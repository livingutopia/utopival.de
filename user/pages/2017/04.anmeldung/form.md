---
title: Anmeldung
# form:
#     name: anmeldungen-2017
#     fields:
#         - name: name
#           label: Name
#           placeholder: 
#           type: text
#           validate:
#             required: true

#         - name: email
#           label: Email
#           placeholder: 
#           type: email
#           validate:
#             required: true

#     buttons:
#         - type: submit
#           value: Absenden
#         - type: reset
#           value: Zurücksetzen

#     process:
#         # - email:
#         #     from: "{{ config.plugins.email.from }}"
#         #     to:
#         #       - "{{ config.plugins.email.from }}"
#         #       - "{{ form.value.email }}"
#         #     subject: "[Feedback] {{ form.value.name|e }}"
#         #     body: "{% include 'forms/data.html.twig' %}"
#         - save:
#             fileprefix: feedback-
#             dateformat: Ymd-His-u
#             extension: txt
#             body: "{% include 'forms/data.txt.twig' %}"
#         - display: bestaetigung

---

# Anmeldung

Das utopival 2017 ist leider schon vorbei. Bald gibts aber wieder ein neues! :) 

Wenn du dir vorstellen kannst, uns bei der Vorbereitung des nächsten utopivals zu unterstützen, dann melde dich doch gerne an [kontakt@utopival.de](mailto:kontakt@utopival.de)