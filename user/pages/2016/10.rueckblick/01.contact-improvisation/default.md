---
bericht: yes
title: "Über den Workshop 'Contact Improvisation' von Manuel K. - Marieke K. (Teilnehmerin)"
---
<h1 class="mb-1">Über den Workshop 'Contact Improvisation' von Manuel K.</h1>
<small>von Marieke K. (Teilnehmerin)</small>

Von Kontakt Improvisation hatte ich gehört, es auch schon mal gesehen und es doch noch nie selbst ausgeführt.

Der Workshop war unheimlich schön, weil er eine Tiefe mit sich brachte, die durch den verbalen Austausch so nicht möglich ist. Manuel vermittelte die Grundlagen dieser Tanzart auf leichtverständliche und rücksichtsvolle Weise, sodass jeder in einem angenehmen Maß die Übungen ausprobieren konnte.

Vor allem die Freude, die ich aus diesen Stunden ziehen konnte, geteilt mit Menschen, die ich zuvor nicht gekannt habe, hinterlassen bei mir den Wunsch wieder mehr zu tanzen, loszulassen, einander zu vertrauen und zu stützen.
