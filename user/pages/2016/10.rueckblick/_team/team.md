---
title: Team
team:
  - name: Lisa
    image: 'lisa.jpg'
  - name: Doreen
    image: 'doreen.jpg'
  - name: Olli
    image: 'olli.jpg'
  - name: Vera
    image: 'vera.jpg'
  - name: Inanna
    image: 'inanna.jpg'
---

## Vorbereitungsteam