---
bericht: yes
title: "Über den Workshop 'Sexualität, Mut, Ehrlichkeit und irgendwo dazwischen Ich...!' von Raphael & Lisa - Marieke K. (Teilnehmerin)"
---
<h1 class="mb-0">Über den Workshop 'Sexualität, Mut, Ehrlichkeit und irgendwo dazwischen Ich...!' von Raphael & Lisa</h1>
<small>von Marieke K. (Teilnehmerin)</small>

Dass bei diesem Workshop das Zauberwiesen-Zelt aus allen Nähten platzte, spricht für sich: Sexualität bleibt ein Thema, dass trotz seiner Allgegenwärtigkeit in unserer Gesellschaft zu selten authentisch und verletzlich thematisiert wird. Hier war das nun möglich. Raphael und Lisa schafften schon innerhalb der ersten Minuten ein intimes und vertrautes Miteinander und wir erkannten, dass wir mit vielen unserer Unsicherheiten nicht allein da stehen, genau so, wie wir feststellen konnten, dass wir alle ganz verschieden Erfahrungen gesammelt haben, unterschiedliche Vorlieben haben und dass all das vollkommen in Ordnung ist.

Es gibt auch hier kein richtig und falsch und letztlich sind wir diejenigen, die kommunizieren und so gemeinsam ein offenes und wohliges intimes Miteinander erleben können.
