---
bericht: yes
title: "Eine reflektierende Feedbackmail ans Team - Joachim (Teilnehmer)"
---
<h1 class="mb-0">Eine reflektierende Feedbackmail ans Team</h1>
<small>von Joachim (Teilnehmer)</small>

Es war eine tolle Woche, teilweise ganz anders als gewohnt, aber ganz viele Sachen haben sich "richtig" angefühlt. Oft wurde mir erst bei bewusstem Reflektieren darüber bzw. im Nachhinein klar, dass etwas Gewohntes (fast) nicht präsent war:
- Geld
- Alkohol
- Zigarettenrauch
- Kohlensäurehaltige Getränke
- Kontakte "nach außen" ( dafür sehr Intensive Kontakte innerhalb unsere Gruppe )
- Nachrichten
- TV, Internet, Telefon..
- negative Gedanken / Gefühle
- ...

Am letzten Abend (Freitag), als beim Konzert die Schüsseln mit den Leckerreien rumgingen, machte ich eine interessante Erfahrung: ich fühlte "Gier" ("wann kommt die Schüssel zu mir? Nehme ich mir auch genug heraus? Wo ist die nächtste Schüssel?") ...ganz un-typisch für diese Woche! Ob das der Zucker war?
Vielleicht ist es eine Idee für das nächste Mal, die Drogenfreiheit auch auf Zucker auszuweiten?!

Ich hoffe, dass ich die vielen Impulse, Eindrücke und Erkenntnisse in meine Leben integrieren und nutzen kann und dass auch von den vielen Menschen eine Verbindung bleiben und wachsen kann, sodass das Utopival nicht nur eine einmalige, schöne Erfahrung war, sondern auch nachhaltig weiterwirken kann.
Nochmal ganz vielen Dank, dass Ihr das alles möglich gemacht habt!

Joachim
