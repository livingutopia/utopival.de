---
title: Rückblick 2016
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _banner
            - _fotos
            - _team
            - _berichte
---