---
bericht: yes
title: "Über den Workshop 'Gerechtigkeit. Identität. Rassismus.' von Nesreen Hajjaj - Marieke K. (Teilnehmerin)"
---
<h1 class="mb-0">Über den Workshop 'Gerechtigkeit. Identität. Rassismus.' von Nesreen Hajjaj</h1>
<small>von Marieke K. (Teilnehmerin)</small>

Nesreen hat uns drei Stunden lang durch einen Dschungel an Selbstreflektion geschickt. Zunächst fanden wir heraus, welcher der Wert in unserem Leben ist, der uns am Wichtigsten scheint. Nicht überraschend blieb vielen von uns nur „Liebe“ als unverzichtbar übrig, doch wenn das so ist, wieso leben wir dann doch oft getrieben von anderen Werten und Erwartungen?

Wir haben uns ausgetauscht, wo für uns persönlich Diskriminierung anfängt, wo es aufhört. Wie wir auf verschiedene Situationen und Menschen reagieren und wie jeder Mensch genau so, wie er ist, integriert werden kann. Wie wir zu gewissen Thesen stehen. Die Diskussionen waren unheimlich interessant und vielfältig. Nesreen war dabei eine tolle „Leitung“ und beantwortete alle Fragen zum Thema Islam sehr offen und ehrlich.
