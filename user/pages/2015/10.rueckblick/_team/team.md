---
title: Team 2015
team:
  - name: Daniel
    image: 'daniel.jpg'
  - name: Pia
    image: 'piad.jpg'
  - name: Tobi
    image: 'amala.jpg'
  - name: Tinka
    image: 'tinka.jpg'
  - name: Lotte
    image: 'lotte.jpg'
  - name: Scarlett
    image: 'scarlett.jpg'
  - name: Carina
    image: 'carina.jpg'
  - name: Marius
    image: 'marius.jpg'

---

## Orgateam