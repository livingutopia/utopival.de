---
elements: /2016/rueckblick
filter: bericht
class: bg-white

---
## Presse

- [Artikel in der Neuen Osnabrücker Zeitung von Yannick Richter (Journalist)](https://www.noz.de/lokales-dk/ganderkesee/artikel/753274/sechs-tage-lang-ohne-geld-und-konsum-in-heide-leben)
- [Videobericht von 'Radio Bremen'](http://www.radiobremen.de/mediathek/index.html?sid=video91168)
- [Bericht im Magazin der WWF Jugend von Lisa St. (Teilnehmerin)](https://www.wwf-jugend.de/durchstarten/aktionen/aktuelles/utopival-eine-woche-utopien-leben-;8950)
- [Bericht von Alina Herr (Referentin des Workshops 'Spielerisch das bedingungslose Grundeinkommen erleben')](https://www.das-gesellschafts-spiel.de/2016/08/28/erst-das-grundeinkommen-und-dann-gar-kein-geld/)
- [Artikel in der 'Kreiszeitung'](https://www.kreiszeitung.de/lokales/oldenburg/ganderkesee-ort58756/utopia-liegt-zwischen-einfamilienhaeusern-6640808.html)