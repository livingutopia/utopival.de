---
title: 'Der Countdown läuft und die letzen Vorbereitungen auch :) '
---

Wir haben uns dieses Wochenende zum letzten offiziellen utopival-Planungswochenende in Ganderkesee zusammen gefunden. Es wurden erste Essensabholungen gemacht, der Keller und andere Orte freigeräumt und auch schon Essen einsortiert. Außerdem entsteht gerade ein Lageplan, für den wir das Gelände erkundet und uns vorgestellt haben, wo welcher wundervolle Ort entstehen soll. Wir sind dabei die Bautage zu planen, die ab nächstem Donnerstag hier stattfinden werden. Hier an diesem wunderschönen Ort zu sein und konkrete Pläne zu schmieden, lässt das utopival in unseren Köpfen schon langsam real werden. Und bald geht es ja auch wirklich los!