---
title: Sommer, Sonne, utopival-Vorfreude
date: 21.7.2019
---
Zwischenzeitlich wurde es etwas ruhiger bei uns im utopival Planungsteam, da einige Menschen auch in der Move Planung involviert waren – aber jetzt werden wir alle mit neuem Elan in die weiteren Vorbereitungen für das utopival starten.
Inzwischen stehen die meisten Referent\*innen für die Workshops, die wir als Rahmenprogramm auf dem utopival anbieten wollen. Einige rechtlichen Fragen wurden geklärt, ein Awareness-Konzept ist in der Metamorphose und wir sind fleißig am Materialien sammeln. Mitte August wird es nochmal ein Planungstreffen in Ganderkesee geben und wir werden uns zu den offenen Bautagen treffen. Sonnige Grüße vom utopival 2019 Planungsteam! 

