---
title: "Teilis wurden ausgelost"
slug: ausgelost
date: "2017-06-06"
author: chandi
---
Zeremoniell haben wir nun die 100 Teilnehmenden und 80 Nachrückende ausgelost. Du solltest eine Email in deinem Postfach finden mit der Info, ob du dabei bist.<br>
Bitte bestätigt eure Teilnahme. Solltest du nicht kommen, würden wir gerne einem anderen Menschen deinen Platz schenken können.

