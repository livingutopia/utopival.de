---
title: Home
hero_image: header2.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items: '@self.children'
    limit: 6
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    description: 'Sample Blog Description'
    limit: 10
pagination: true
forms:
    newsletter-form:
        name: newsletter
        fields:
            spacer:
                type: spacer
                text: 'Bleib auf dem Laufenden und trag Dich in den Newsletter ein'
                classes: text-small
            email:
                type: text
                placeholder: E-Mail
                display_label: false
                size: x-small
                validate:
                    required: true
        buttons:
            submit:
                type: submit
                value: abonnieren
        process:
            -
                mailtrain_subscribe:
                    host: 'https://mails.utopival.de'
                    list_id: rJRiRtW4G
                    require_confirmation: true
                    email: '{{ form.value.email }}'
            -
                message: '<b>Wunderbärchen!</b><br /> Du erhälst nun eine E-Mail mit einem Link, mit welchem du dein Abonnement bestätigen kannst.'
---

