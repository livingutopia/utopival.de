---
title: 'Verlängerte Anmeldefrist'
author: Anja
---
Vergangenes Wochenende haben wir uns vom Planungsteam wieder im Kanthaus bei Leipzig getroffen und weiter am utopival 2019 gebastelt! Unter anderem haben wir am Programm gewerkelt, das jetzt endlich konkrete Gestalt annimmt. Wir haben schon einige feste Zusagen von Referent\*innen - Ihr dürft euch auf spannende und vielseitige Workshops freuen! 

Noch sind einige Plätze frei, also zögert nicht und meldet euch an! Wir verlängern die Anmeldefrist noch bis zum 13. Juni. Erzählt auch gerne anderen interessierten Menschen vom utopival! Wir würden uns über ein diverses utopival freuen - Menschen aus allen Blasen und Phasen sind herzlichst willkommen!

