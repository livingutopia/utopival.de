---
title: "Freitag: Ruhe im Wandel"
slug: ruhe-im-wandel
date: "2017-07-28"
author: chandi
hero_image: aufbau_fr.jpg
---
Überall laufen Menschen umher, suchen Werkzeug, bauen Bühnen, schnibbeln Essen, sammeln Pilze, dekorieren Bungalows, malen Schilder,... - aber dennoch: eine angenehme Ruhe herrscht.

