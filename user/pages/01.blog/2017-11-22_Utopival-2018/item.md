---
title: "Utopival 2018 - neues Team gesucht"
slug: Utopival-2018
date: "2017-11-22"
author: findus & bakira
---
Huhuu ihr Lieben!
Das Planungsteam des letzten utopivals sitzt gerade beisammen und stellt sich die Frage: Soll es nächstes Jahr wieder ein utopival geben?
Wir fühlen ein riesengroßes JAA! :)

Daher: Hast Du Lust, im Planungsteam 2018 mitzuwirken, deine Ideen & Talente zu teilen?
Schreib uns gerne an [kontakt@utopival.de](mailto:kontakt@utopival.de)

