---
title: 'Die Anmeldung ist geöffnet'
---

Unser 3. Vorbereitungstreffen ist gerade in vollem Gange und neben vielen Diskussionen zum Thema Tauschlogikfreiheit, den potenziellen Orten des Utopivals und den Werten, haben wir es jetzt geschafft, die Anmeldephase feierlich zu eröffnen. Das heißt für euch: Schnell an die Rechner und den Anmeldebogen ausfüllen! 
Ende Mai werden werden wir dann die 100 Menschen auslosen, die beim Mitmachkongress dabei sein können. 
So ganz nebenbei haben wir uns übrigens auch für einen Ort entschieden und können nun stolz verkünden, dass das utopival 2019 vom 1-7.9. in Ganderkesee in der Nähe von Bremen stattfinden wird. 
Dort ist es wunderschön blabla
Wir freuen uns über eure Anmeldungen und sind offen für Ideen zu Workshops und weiteren Vorschlägen die den Mitmachkongress 2019 wieder zu einem einzigartigen, unvergesslichen Erlebnis machen werden. 
Viele liebe Grüße und bis bald, 
euer utopival Vorbereitungsteam 2019
