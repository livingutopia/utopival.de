---
title: 'Startschuss für die Utopival Planung 2019'
author: Anja
published: true
---

Dieses Wochenende kam das neue Planungsteam für das Utopival 2019 zum ersten Mal im Kanthaus bei Leipzig zusammen, um mit den Vorbereitungen für den schönsten utopischen Mitmachraum der Welt zu beginnen. Wir haben uns kennen gelernt, gemeinsam Werte festgelegt, Visionen gesponnen und bereits erste Ideen für potenzielle Orte und Themen, die wir behandeln wollen, gesammelt. Das Utopival 2019 soll ein Ort werden, an dem sich Menschen herzlich und offen begegnen, an dem jede\*r gesehen wird und Wertschätzung erfährt, an dem wir achtsam miteinander und mit unserer Umwelt umgehen. Wir streben ein tauschlogikfreies Miteinander an, in dem jede\*r sich frei nach ihren/seinen Bedürfnissen und Fähigkeiten einbringen kann. 
Falls Ihr noch einen schönen Ort in der Natur kennt, an dem Platz für etwa 100 Menschen wäre und den wir im Sommer geldfrei nutzen könnten, freuen wir uns über Eure Vorschläge

![](brainstorming.jpg)
