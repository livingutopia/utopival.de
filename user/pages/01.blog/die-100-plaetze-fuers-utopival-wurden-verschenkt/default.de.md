---
title: 'Neuigkeiten aus Ganderkesee '
---

Ihr lieben Menschen,

Ein kurzes aber produktives Planungstreffen liegt hinter uns. Dieses Mal haben wir uns in Ganderkesee getroffen, wo das utopival dieses Jahr stattfindet. Nicht nur die Familie die hier wohnt, hat uns super lieb beherbergt, auch das Gelände ist wirklich wunderschön. Wir sind übers Gelände spaziert und haben zwischen Vogelgezwitscher und Wellengeplätscher überlegt, wo wir schlafen, kochen, duschen und voneinander lernen werden. Außerdem sind wir ganz aufgeregt, weil jetzt feststeht, wer dieses Jahr zum utopival kommen wird. Ihr bekommt in den nächsten Tage eine Email mit der Teilnahmebestätigung von uns. 

Die Vorfreude aufs utopival wächst! Behaltet euer Email-Postfach im Auge, es wird bald spannende Infos zum Mitmachen für dieses Jahr geben.

Liebst

eure utopival Crew


