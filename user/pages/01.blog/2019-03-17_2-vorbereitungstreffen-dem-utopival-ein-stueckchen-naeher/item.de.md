---
title: '2. Vorbereitungstreffen: Dem utopival ein Stückchen näher'
---

Das Wochenende vom 15.-17. März haben wir gemeinsam in Berlin verbracht. Wir haben nicht nur viel Tee getrunken und eine wunderschöne Zeit zusammen gehabt, sondern unsere Ideen fürs utopival haben sich auch zu etwas Festerem, Konkreterem entwickelt.

Wir haben uns über die Orte und den bisherigen Planungsstand ausgetauscht, an denen das Utopvial stattfinden könnte. Außerdem haben wir verschiedene Planungsphasen mit dazugehörigen Aufgabenbereichen erarbeitet und aufgeteilt. Viel unserer Energie ist auch ins Sammeln und Filtern von spannenden, abwechslungsreichen Themenideen für Workshops geflossen. Bald können wir damit beginnen, potenzielle Referent\*innen anzufragen. Wir freuen uns, dass es dieses Jahr auf dem utopvial einen Kids-space geben wird und  herumtobende Kinder alles hoffentlich noch bunter und schöner machen werden. Langsam aber sicher nimmt es Form an! 

Bis Anfang April wollen wir den Ort festlegen an dem das utopival diesen Sommer stattfinden wird. Falls dir ein Ort einfällt an dem 120 Menschen tauschlogikfrei eine Woche verbringen können melde dich gern bei uns! Wir wünschen uns einen Wasseranschluss, sonst ist alles offen.

Utopische Grüße und bis bald
Euer Vorbereitungsteam.