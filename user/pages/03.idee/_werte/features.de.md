---
class: 'small text-center'
features:
    -
        header: tauschlogikfrei
        icon: 'fa fa-asterisk'
        text: 'Wir wollen durch unsere Projekte eine Entkopplung von Geben und Nehmen hin zum Teilen ausprobieren und erfahrbar werden lassen.'
        url: 'https://livingutopia.org/geldfrei/'
    -
        header: vegan
        icon: 'fa fa-heart'
        text: 'Die vegane Lebensweise vereint ökologische, ethische, politische, soziale, wirtschaftliche und gesundheitliche Aspekte.'
        url: 'https://livingutopia.org/vegan/'
    -
        header: drogenfrei
        icon: 'fa fa-eye'
        text: 'Jeder Mitmachraum ist ein drogenfreier Ort. Wir möchten darauf Rücksicht nehmen, dass nicht alle Menschen Drogen konsumieren oder mit Drogenkonsum in Kontakt kommen möchten und uns für die Zeit der Mitmachräume von der Gewohnheit, Drogen zu konsumieren, lösen.'
        url: 'https://livingutopia.org/drogenfrei/'
    -
        header: ökologisch
        icon: 'fa fa-leaf'
        text: 'Wir haben den Anspruch, unsere Mitmachräume und Handlungen so ökologisch wie möglich zu gestalten.'
        url: 'https://livingutopia.org/oekologisch/'
    -
        header: solidarisch
        icon: 'fa fa-globe'
        text: 'Leistungsdruck, Konkurrenzkampf und ungerecht verteilte Privilegien sind fest in der aktuellen Gesellschaft verankert. In unseren Mitmachräumen hingegen möchten wir miteinander kooperieren, voneinander lernen, und uns kritisch mit unseren Privilegien und unserer gesellschaftlichen Rolle auseinander setzen und diese letztendlich zu überwinden versuchen.'
        url: 'https://livingutopia.org/solidarisch/'
---

<br />
# Motive

Wie alle unsere Projekte vom Projekt- und Aktionsnetzwerk [living utopia](http://www.livingutopia.org) wird der Mitmachkongress utopival nach den folgenden begleitenden Motiven organisiert und verwirklicht:
