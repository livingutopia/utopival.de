---
title: 'Idee Text'
image_align: right
class: bg-gray
---

<span style="float:right">![](logoround.png "logo")</span>

<h1 class="mb-1" style="font-size: 4em">utopival</h1>
<h4 class="mt-0">ein Mitmachkongress, 6 Tage, ca.100 Menschen</h4>

<p></p>

Und eine zentrale Frage: “Wie stellen wir uns eine zukunftsfähige Gesellschaft von morgen vor?” Wir möchten gemeinsam einen Mitmachraum schaffen, um utopiegtaugliche Alternativen zu diskutieren und zu leben. Zusammen mit kreativen, kritischen und motivierten Menschen verwirklichen wir dieses Jahr zum vierten Mal an einem wundervollen Ort das utopival!

2019 werden wir wieder eine Woche lang gemeinsam neue Wege in Richtung einer zukunftsfähigen Gesellschaft gehen. Diese Tage sind eine Möglichkeit, mal außerhalb der (noch) etablierten Konventionen zu fühlen, zu denken und zu leben: Das utopival lädt zum Träumen ein und dazu, die Idee hinter living utopia zu erfahren. Sei auch mit dabei und lebe deine Utopie!</p>


Wir möchten zeigen, dass wir in einer globalisierten Welt mit lokalen, konsequenten Ansätzen viel bewirkt werden kann: Globale Zusammenhänge – lokale Lösungen. Deswegen ist es uns wichtig, auf der einen Seite gesellschaftskritisch Missstände zu beleuchten, aber vor allem über utopietaugliche Alternativen für eine zukunftsfähige, diskriminierungsfreie, solidarische Welt von morgen nach zu denken. Dafür ist es notwendig, dass die eigene, gesellschaftliche Position sowie unser Handlungsspielraum auf verschiedenen Ebenen reflektiert wird. Hierfür soll Raum beim utopival sein.

Dabei ist es wichtig, die Utopie nicht als neue ‘Gesellschaftsform’ zu verstehen, sondern als Wegweiserin, als Motivation und Prozess. Die Utopie ist außerdem ein Freiraum, in dem Bedenken wie »Das kann ich mir nicht vorstellen!« oder »Das hat noch nie funktioniert!« keinen Platz haben. Dazu möchte das utopival einladen.

Die tauschlogikfreie Organisation ist dabei für uns ein gesellschaftliches Experiment, mit welchem wir Ansätze einer Ökonomie des Teilens schon heute leben möchten. Durch das Überwinden des Prinzips “Leistung – Gegenleistung” möchten wir neue Wege in ein wirtschaftliches und gesellschfatliches Miteinander gehen.

<center>
<iframe src="https://www.youtube-nocookie.com/embed/RwKi6gZZKtU" width="640" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</center>