---
title: Anmeldung
form:
    name: anmeldung-2019
    fields:
        -
            type: spacer
            title: 'Ein paar Daten...'
        -
            name: name
            label: Name
            placeholder: asd
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: null
            type: email
            validate:
                required: true
        -
            name: alter
            label: 'Ungefähres Alter'
            type: select
            options:
                - 0-13
                - 14-23
                - 24-34
                - 35-44
                - 45-54
                - 55-69
                - 70-100
                - 100+
            validate:
                required: true
        -
            name: ort
            label: 'Wohnort oder momentaner Lieblingsort'
            placeholder: null
            type: text
            validate:
                required: true
        -
            name: utopivals
            label: 'Warst du schonmal auf einem Utopival?'
            type: select
            options:
                - 'Noch nie'
                - 'Ein mal'
                - 'Zwei mal'
                - 'Drei mal'
                - 'Vier mal'
            validate:
                required: true
        -
            name: anmeldungen
            label: 'Hast du dich schonmal angemeldet?'
            type: select
            options:
                - 'Noch nie'
                - 'Ein mal'
                - 'Zwei mal'
                - 'Drei mal'
                - 'Vier mal'
            validate:
                required: true
        -
            name: sonstiges
            label: 'Hast du Allergien, Lebensmittelunverträglichkeiten, Einschränkungen in deiner Mobilität, Behinderungen oder sonstige Punkte, die das Orgateam wissen sollte?'
            type: textarea
            rows: 3
        -
            name: lastminiute
            label: 'Möchtest du auf die Last-Minute-Nachrückliste, die wir am ersten Kongresstag am 1. September kontaktieren, falls andere Teilnehmer*innen unvorhergesehen nicht angereist sind?'
            type: radio
            default: Nein
            options:
                Ja: Ja
                Nein: Nein
            validate:
                required: true
        -
            type: spacer
            title: Reflexion
            text: 'Kein Sorge, wir werden nicht die besten&orginellsten Antworten herraussuchen. Deine Antworten werden gar nicht bewertet. Wir möchten eigentlich nur, dass du herausfindest, ob du beim utopival am richtigen Ort wärst, und dir mit dem Formular nebenher auch noch einen interessanten, (selbst)reflexiven Moment anbieten. Falls du das nicht magst, kannst du auch den kurzen Weg wählen.'
        -
            name: motivation
            label: 'Was ist Deine Motivation, am Mitmachkongress utopival teilzunehmen?'
            type: textarea
            rows: 3
        -
            name: vision
            label: 'Wenn das utopival eine Miniaturausgabe unserer Welt von morgen wäre, was müsste der Mitmachkongress dann für dich beinhalten? Was wünschst du dir vom utopival?'
            type: textarea
            rows: 3
        -
            name: themen
            label: 'Welche persönlichen, gesellschaftlichen oder sonstigen Fragen beschäftigen dich im Moment? Welche Themen bringst du mit aufs utopival?'
            type: textarea
            rows: 3
        -
            name: ohne_geld
            label: 'Wenn Geld keine Rolle mehr spielen würde, was würdest du dann im Leben am liebsten tun?'
            type: textarea
            rows: 3
        -
            name: geteilt
            label: 'Wann hast du das letzte mal etwas geteilt oder verschenkt und was war es?'
            type: textarea
            rows: 3
        -
            name: geteilt
            label: 'Mit welchen Tricks oder kleinen Umstellungen im Alltag könntest du dir vorstellen die Welt zum Besseren zu verändern?'
            type: textarea
            rows: 3
    buttons:
        -
            type: submit
            value: Absenden
    process:
        -
            email:
                from: anmeldeformular@utopival.de
                to:
                    - kontakt@utopival.de
                subject: '[Anmeldeformular] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: anmeldung-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            mailtrain_subscribe:
                host: 'https://mails.utopival.de'
                list_id: 8EKZ7vQcn
                require_confirmation: false
                email: '{{ form.value.email }}'
        -
            display: bestaetigung
---

# Anmeldung 2019

Jeder Platz beim utopival ist ein großartiges Geschenk an dich, welches wir gemeinsam mit Freude und Stolz und auch mit sehr viel Orga-Aufwand 100 Menschen ermöglichen. Hoffentlich auch dir! Wenn du dich dazu entscheidest dich anzumelden, wünschen wir uns daher eine größtmögliche Verbindlichkeit und im Idealfall keine spontanen Absagen oder sogar nicht abgesagtes Nicht-Kommen! 

#### Wichtig
- Der eigentliche Anmeldezeitraum endete am 13. Juni. Du kannst dich weiterhin anmelden, landest damit auf der Nachrückliste und erhältst von uns Bescheid, sollte ein Platz für dich frei werden. 

#### Hinweise zur Anmeldung
- Wir wünschen uns für die Atmosphäre deine Anwesenheit an allen 7 Tagen.
- Mitmachkongress bedeutet, dass du - genauso wie alle anderen - zur Gestaltung der gemeinsamen Zeit beiträgst (beispielsweise in Form von Hilfe beim Kochen, Aufräumen, spontanen Diskussionsrunden, Anregungen und dergleichen) Ganz nach dem Prinzip "wenn du eine Aufgabe siehst, die erledigt werden möchte, ist sie deine!"
- Die Infrastruktur unseres diesjährigen Ortes bringt einige Gegebenheiten mit sich, derer du dir bewusst sein solltest: Schlafen im Zelt oder Wohnwagen bzw. in einem großen Schlafraum (bei besonderer Empfindlichkeit kann auf uns zugekommen werden), eingeschränkter Strom- und Warmwasserzugang und ökologische Komposttoiletten anstelle von herkömmlichen Wassertoiletten.
- Als Teilnehmer\*in wirst auch du eingeladen, dich während des utopivals an den begleitenden Motiven zu orientieren: solidarisch, ökologisch, vegan und tauschlogikfrei. Außerdem möchten wir einen drogenfreien Erfahrungsraum schaffen, in dem du dich an Natur und menschlichem Miteinander berauschen kannst.
- Eine Anmeldung ist nur für Einzelpersonen möglich (abgesehen von Babies/Kindern). Dies haben wir so entschieden, da wir angesichts der wenigen Plätze so fair wie möglich verlosen möchten und auch, weil der erwünschte intensive, gruppendynamische Prozess durch mehrere gemeinsam angemeldete “Grüppchen” gehemmt werden könnte. Wir hoffen sehr auf euer Verständnis.

