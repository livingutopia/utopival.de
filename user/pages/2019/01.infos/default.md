---
title: Infos
---

[TOC]

<h2 style="font-size: 1.6rem">Praktische Infos zum utopival 2019</h2>

### <i class="fa fa-clock-o"></i> Zeit
Der Mitmachkongress utopival geht vom 1. - 7. September 2019. Wir möchten eine intensive Gruppendynamik schaffen, daher wünschen wir uns von allen Anwesenheit über die gesamte Zeit. Während des utopivals gibt es drei gemeinsame Mahlzeiten am Tag und ein Angebot an Aktivitäten zu verschiedenen Uhrzeiten, es ist aber auf jeden Fall auch einige Freizeit in die Tagesplanung eingebracht. 😉

### <i class="fa fa-location-arrow"></i> Ort
Dieses Jahr findet das utopival – wie auch schon 2016 - in Ganderkesee bei Bremen statt. Das malerische Privatgrundstück mit den vielen alten Bäumen, riesigen Wiesen mit hohem, im Wind wehenden Graß, vielen alten Bäumen und verwunschenen Ecken läd zum Träumen ein. Inmitten all dem Platz den wir haben werden um neue Utopien zu spinnen, liegt das Herz des Grundstücks: Ein kleiner See mit Strand und Steg. Auf dem Gelände befinden sich viele zauberhafte Dinge wie beispielsweise eine Telefonzellenwasserstelle, ein Beachvolleyballfeld, ein Hängemattenwäldchen, Slacklinebäume, und viele Ecken zum Entdecken. Wir werden in den Aufbautagen noch viel Deko hinzuzaubern und den Ort der Stille, die Kochecke mit den Feuerstellen und vieles mehr. 

Aus Rücksicht auf unsere Gastgeberinnen teilen wir die genaue Adresse nur den ausgelosten Teilnehmer\*innen und Referierenden vertraulich mit.

### <i class="fa fa-bed"></i> Unterbringung
Unser Gelände hat eine wunderschöne Wiesenfläche, auf der in selbstmitgebrachten Zelten (oder natürlich auch unter offenem Himmel) übernachtet werden kann. Auch für Hängematten gibt es Aufhängmöglichkeiten, und auch Parkplatzfläche für Wohnwagen und andere Übernachtungsgefährte. 
Anders als im letzten Jahr können wir leider – auch für Referent\*innen – keine Unterbringung in Innenräumen oder Betten ermöglichen, die vorhandenen Gebäude bieten diese Kapazitäten nicht.

![](zelte.jpg)

### <i class="fa fa-spoon"></i> Verpflegung
Mit Stolz können wir hier schreiben: Für sämtliche Teilnehmenden gibt es für die volle Zeit des Kongresses geldfreie, vegane Vollverpflegung! Wir bereiten gemeinsam drei Mahlzeiten am Tag, in denen auch auf Allergien, Unverträglichkeiten und besondere Wünsche konkret Rücksicht genommen werden kann. Die Mahlzeiten für 6 Tage für 130 Menschen werden sich zusammenstellen aus geretteten und gespendeten, großteils biologischen Lebensmitteln, die wir mit Mühe und Liebe über Monate zusammenorganisieren und bei deren Zubereitung vor Ort unsere Köch\*innen von allen Teilnehmenden unterstützt werden.
Das gemeinsame Kochen und Schnibbeln ist einer der wichtigen Mitmachaspekte dieses Mitmachkongresses und fördert neben dem Gruppengefühl auch die Wertschätzung für die luxuriösen Köstlichkeiten, die wir genießen werden – und macht außerdem einfach Spaß! Die besten Gespräche sind bis jetzt fast immer beim Schnibbeln entstanden 😀

![](mampf.jpg?height=300)

### <i class="fa fa-shower"></i> Toiletten
Auf dem diesjährigen Gelände werden keine herkömmlichen Wassertoiletten zur Verfügung stehen, sondern ausschließlich ökologische Komposttoiletten, die für den Kongress auf dem Gelände verteilt angelegt werden. Diese sind hygienisch einwandfrei, umweltfreundlich, geruchsarm (aufgrund der Tiefe der Gruben und der Abdeckmaterialien), blickdicht, außerdem ausgestattet mit normalem Toilettenpapier und liegen jeweils etwas abseits. Wir weisen darauf hier schon hin, da vielleicht für manche Teilnehmenden die ausbleibende Wasserspülung gewöhnungsbedürftig ist.

### <i class="fa fa-shower"></i> Duschen
Es wird neben dem See (in dem gebadet werden kann, auch wenn er sehr algig ist) auch einige Eimerduschen geben. Es gibt nur eingeschränkt Zugang zu Warmwasser. Da das Wasser direkt in den Boden fließt haben wir die dringende Bitte an alle AUSSCHLIESSLICH (!) ökologisch abbaubares Shampoo und Duschgel oder ökologisch abbaubare Seife, sowie Zahnpasta zu verwenden!

### <i class="fa fa-fire"></i> Feuer
Das Gelände hat viele Grasflächen mit tendenziell trockenem Stroh und anderen Brandmaterialien, daher ist leider jede Art von Feuer außerhalb von abgesicherten Stellen verboten. Das bedeutet, keine Kerzen, Fackeln, Feuerpois, Zigaretten, … Da es auf dem Grundstück schon mal gebrannt hat, bitten wir alle sich daran unbedingt zu halten. 
Jedoch dürfen sehr gern alle Arten von elektrischem Licht mitgebracht werden (Lichtpois, LED-Lämpchen, Taschenlampen...), da wir abends auch alles schön beleuchten möchten, so gut es eben ohne Fackeln geht.

### Tiere
Falls du nicht-menschliche Begleiter\*innen hast, wäre eine anderweitige Unterbringung während des utopivals sinnvoll, denn es können keine Tiere mitgebracht werden. Es tut uns sehr leid in diesem Aspekt 'ausschließen' zu müssen, doch dies ist im Sinne der gastgebenden Familie und auch des Teams aufgrund von Allergien, Risiken, anwesenden Kindern und ungesichertem Gelände, auf welchem bereits Tiere leben.

### Drogenfrei
Wir wollen während dieser sechs Tage unseren Fokus auf intensiven zwischenmenschlichen Austausch und auf gemeinsame Erfahrungen legen und möchten dafür auf dem Gelände einen möglichst drogenfreien Raum schaffen, in dem sich keine durch Drogen bedingten Barrieren für Menschen ergeben. Es geht uns dabei nicht darum dogmatisch zu sagen, dass Drogen schlecht sind, sondern darum „normale“ Selbstverständlichkeiten zu hinterfragen und auch um ein Gefühl der Sicherheit und Echtheit, welches durch Drogen in vielen Kontexten eingeschränkt werden kann. Kaffee, Tee, Schokolade, Zucker oder Ähnliches schließen wir da nicht mit ein, und falls du rauchen oder ein Bier trinken willst, darfst du das auch, aber dann bitte einfach außerhalb des Geländes. Oder vielleicht ist es ja sogar interessant dich an diesen sechs Tagen ganz davon zu lösen? 

### Technische Geräte
Du bist eingeladen, für die Zeit des utopivals alle technischen Geräte zu pausieren und sofern du dich wohl damit fühlst alle nicht benötigten Handys, Laptops, Smartphones, Tabletts, Kameras und was es noch alles gibt zuhause zu lassen. Der Empfang ist sowieso nicht so gut und es taucht sich viel schöner in eine utopival Blase ein, wenn du nicht zur Hälfte in der 'Außenwelt' steckst. ;-)
Wertgegenstände können bei uns im Teamraum eingeschlossen werden. 

### Kinder
Liebe Eltern und Kinder, das utopival ist als Ort und Event für alle Altersstufen offen und wir freuen uns sehr auf alle. Insgesamt werden nicht viele Kinder da sein und es gibt auch kein explizites Kinderprogramm, immerhin aber viel Spielzeug, eine Rutsche, Schaukel, den kleinen Strand, Sand zum Buddeln und natürlich viele liebe, verspielte, sonnige und kinderliebe Menschen. Tragt gerne eure Fragen, Bedürfnisse und Wünsche an uns heran!

### Awareness
Es wird auf dem utopival ein Awarenessteam geben.
Unter Awareness verstehen wir ein machtkritisches Bewusstsein für die eigene Position sowie ein Bewusstsein dafür, in zu verändernden und veränderbaren Strukturen zu stecken – In welchen Situationen bin ich selbst privilegiert und wie kann ich das vielleicht ändern? Unsere gesellschaftliche Position wird von strukturellen Machtverhältnissen mitbestimmt. Menschen, die gesellschaftlich privilegiert sind, haben es häufig(!) leichter; andere, die öfter Diskriminierung erleben, haben es häufig(!) schwerer. Die unterschiedliche Positioniertheit muss sichtbar gemacht werden, wenn eine Veranstaltung möglichst angenehm für alle Beteiligten ablaufen soll. Awareness versucht, das Bewusstsein für individuelle und strukturelle Ungleichheiten zu schärfen und produktiv mit diesen umzugehen. Awareness-Arbeit hat also das Ziel, mit und für alle Beteiligten diskriminierungsfreie(re) und somit sozial-gerechtere Räume herzustellen.
Awareness ist für uns außerdem eine Voraussetzung für den eigenen Handlungsspielraum: Erst wenn ich mir über etwas bewusst bin, kann ich ein Problem beheben. Konkret bedeutet das für uns auch, Achtsamkeit und Zuhören bei Bedürfnissen anderer auf dem utopival zu fördern. Achtsamkeit kann im direkten Austausch stattfinden. Du kannst Dich selbst fragen, was Du brauchst, um Dich wohl zu fühlen. Sich über eigene Bedürfnisse bewusst zu werden und diese transparent zu kommunizieren, kann ein erster Schritt sein.
Wir freuen uns darauf mit euch gemeinsam auf dem utopival eine angenehme Atmosphäre zu schaffen, in der sich Jede\*r wohlfühlen kann!

### Eigene Haftung
Die Teilnahme am utopival findet auf rechtlicher Ebene auf eigene Verantwortung statt, wir vom Orgateam können nicht haften.

### Mitbringliste
- Zelt und/oder Hängematte, Schlafsack, Isomatte
- Stirnlampe oder Taschenlampe (sehr empfehlenswert, da bei Dunkelheit nicht alle Wege auf dem weitläufigen Gelände ausgeleuchtet werden können)
- Trinkflaschen
- Teller und/oder Schüssel, Besteck, Becher/Flasche (Ein bisschen was haben wir auch da, falls es wer vergisst. Trinkflasche aber unbedingt mitbringen!)
- warme und regenfeste Kleidung für alle Fälle
- Handtuch, Badesachen
- ökologisch abbaubares Shampoo/Duschgel/Seife (bio ist nicht gleichzusetzen mit ökologisch abbaubar!!!)
- Sitzunterlage (für feuchten Boden – wir werden uns bemühen Pappen zu organisieren, aber für sensible Hinterteile kann eine eigene Unterlage während der Workshops ein Segen sein)
- Handtuch


##### Außerdem gerne
- Jonglagezeug, Musikinstrumente, Slacklines, Hängematte, ... (es gibt viele tolle Bäume!)
- Beitragidee für die Open Stage
- Zu Verschenkendes für die Verschenke-Ecke
- Deko fürs Gelände
- Insektenspray, Sonnencreme
- Fantastische Laune, Ideen und alles, was du sonst noch so zum Wohlfühlen brauchst

##### Was gern zuhause bleiben darf
- Nicht-ökologisches Duschgel und Shampoo
- Technische Geräte aller Art und Drogen, sofern du Lust hast in dieser Woche frei davon zu sein (und wenn dann 'körpereigenen Rausch' zu erleben)
- Gedanken an zu erledigende Hausarbeiten, Jobgespräche, To-Do-Listen für die kommende Woche oder überquellende Planer... ;-)

