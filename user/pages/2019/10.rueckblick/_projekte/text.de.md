---
title: 'Vorgestellte Projekte'
---

## Vorgestellte Projekte
Am utopival konnten im Rahmen einer Vernetzungsveranstaltung Menschen ihre Projekte vorstellen:
- [Kanthaus in Wurzen](https://kanthaus.online) (chandi, findus)
- [Free the Soil](https://freethesoil.org/) (She)
- [living utopia](https://livingutopia.org) (chandi, tobi*)
- [extinction rebellion](https://extinctionrebellion.de)
- [Klimacamp Chiemsee](klimacamp-chiemsee.de/) (Lea)
- [Funkenhaus in Greene](https://gelebteutopie.de) (tobi*)
- [Zähne putzen](https://aktivisti-retreat.org/ ) (Eileen)
- ... Vollständige Liste wird nachgereicht ...