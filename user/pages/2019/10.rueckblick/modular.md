---
title: Rückblick 2019
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _banner
            - _fotos
            - _berichte
            - _team
            - _projekte
            # - _berichte
    #         - _team
    #         - _kontakt
    #         - _haendeundkoepfe
---