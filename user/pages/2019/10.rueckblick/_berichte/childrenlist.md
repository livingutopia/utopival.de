---
elements: /2019/rueckblick
filter: bericht
---
## Presse
- [Delmenhorster Kreisblatt: "Wie in Ganderkesee eine Woche lang ohne Geld und Konsum gelebt wird"<br>*Christopher Bredow*](https://www.noz.de/lokales-dk/ganderkesee/artikel/1866781/wie-in-ganderkesee-eine-woche-lang-ohne-geld-und-konsum-gelebt-wird)

## Texte
Bisher sind noch keine Texte eingegangen. Schreib doch gerne über deine Erfahrungen und schick sie uns an [kontakt@utopival.de](mailto:kontakt@utopival.de)! :)