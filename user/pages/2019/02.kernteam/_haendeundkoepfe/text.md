---
class: bg-white
---
## Von Händen und Köpfen

Um eine reibungslose Bewegung auszuführen brauchen wir Köpfe die den Überblick behalten, sowie Hände, die Gedanken in Taten umsetzen. Und beides ist gleich wichtig. Deswegen verstehen wir - das "Kern-Team" - uns als Köpfe, die durch viele helfende Hände unterstützt werden. Und die seid ihr! Wer also schon jetzt Lust hat mitzudenken oder mitzuwuppen, ist herzlichst dazu aufgefordert und eingeladen sich bei uns zu melden! Je weiter die Zeit voranschreitet, desto mehr freuen wir uns über eure Unterstützung und werden mit diversen Dingen auf euch zukommen - schließlich sind wir alle ein großes Team aus Menschen, die gemeinsam auf dem Weg in eine zukunftsfähige Gesellschaft sind.