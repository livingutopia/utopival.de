---
title: Team
team:
    -
        name: findus
        email: findus@livingutopia.org
        image: findus.jpg
    -
        name: chandi
        email: chandi@livingutopia.org
        image: chandi.jpg
    -
        name: Hannah
        image: hannah.jpg
    -
        name: Anja
        image: anja.jpg
    -
        name: Nia
        image: nia.jpg
    -
        name: Flora
        image: flora.png
    -
        name: Jacky
        image: jacky.jpg
---

## Vorbereitungsteam 2019
